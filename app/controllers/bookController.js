const db = require('../configs/db');
const User = db.user;
const Book = db.book;
const Comment = db.comment;
const asyncMiddleware = require("express-async-handler");

exports.createbook = asyncMiddleware(async (req, res) => {
    const book = await Book.create({
        title: req.body.title,
        author: req.body.author,
        pages: req.body.pages,
        language: req.body.language
    });
    res.status(201).send({
        status: "book has been added"
    });
});

exports.books = asyncMiddleware(async (req, res) => {
    const book = await Book.findAll({
        attributes: ["id", "title", "author", "pages", "language"]
    });
    res.status(200).json({
        description: "All Book",
        book: book
    });
});

exports.book = asyncMiddleware(async (req, res) => {
    const book_id = req.params.id;

    const book = await Book.findOne({
        where: {
            id: book_id
        },
        include: [
            {
            model: Comment,
            attributes: ["id", "comment"],
            include: [{
                 model: User,
                attributes: ["username"]
            }
            ]
            },

        ]
    })
    if(!book){
        res.status(404).send({
            message: 'Id not found'
        })
    }else{
        res.status(200).send(book)
    }
    
});

exports.update = asyncMiddleware(async (req, res) => {
    const book_id = req.params.id;
    
    const { title, author, pages, language } = req.body;

    const book = await Book.update({
        title: req.body.title,
        author: req.body.author,
        pages: req.body.pages,
        language: req.body.language
    },
    {
        where: {
            id: book_id
        }
      }
    )
    if(!book_id){
        res.status(404).send({
            error: true,
            message: 'Id not found'
        })
    }else{
        res.status(201).send({
            error: false,
            message: 'Data has been updated'
        })
    }
});

exports.deletebook = asyncMiddleware(async (req, res) => {
    const book_id = req.params.id;

    const book = await Book.destroy({
        where: {
            id: book_id
        }
    })

    if(!book){
        res.status(404).send({
            message: 'Id not found'
        })
    }else{
        res.status(200).send({
            message: 'book has been deleted'
        })
    }
});
