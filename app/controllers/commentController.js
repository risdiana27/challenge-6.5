const db = require('../configs/db');
const User = db.user;
const Book = db.book;
const Comment = db.comment;
const asyncMiddleware = require('express-async-handler');

exports.comment = asyncMiddleware(async (req, res) => {
	const bookId = req.params.id;

		const comment = await Comment.create({
			userId: req.userId,
			bookId: bookId,
			comment: req.body.comment
		});
		res.status(201).send({
			status: comment
		})
	
		res.status(404).send({
			status: "Book ID not found"
		})
	
})

exports.getComment = asyncMiddleware(async (req, res) => {
    const comment = await Comment.findAll({
        attributes: ["id", "comment"],
        include: [
            {
            model: User,
            attributes: ["id", "username"]
          },
          {
            model: Book,
            attributes: ["id", "title"]
          }
        ]
    });
    res.status(200).json({
        description: "All Comment",
        comment: comment
    });
});

exports.getCommentByIdBook = asyncMiddleware(async (req, res) => {
    const book_id = req.params.id;

    const comment = await Comment.findAll({
        where: {
            bookId: book_id
        },
        include: [
            {
            model: User,
            attributes: ["id", "username"]
          },
          {
            model: Book,
            attributes: ["id", "title"]
          }
        ]
    });
    res.status(200).json({
        description: "Comment By Book Id",
        comment: comment
    });
});

