const verifySignUp = require("./verifySignUp");
const authJwt = require("./verifyJwtToken");
const authController = require("../controllers/authController.js");
const userController = require("../controllers/userController.js");
const bookController = require("../controllers/bookController");
const orderController = require("../controllers/orderController");
const commentController = require("../controllers/commentController");

module.exports = function(app) {
  // Auth
  app.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUserNameOrEmail,
      verifySignUp.checkRolesExisted
    ],
    authController.signup
  );
  app.post("/api/auth/signin", authController.signin);

  // get all user
  app.get("/api/user",[authJwt.verifyToken], userController.users);
  
  //show profile
  app.get("/api/user/profile",[authJwt.verifyToken], userController.showProfile);

  // update user
  app.put("/api/user/profile/edit", [authJwt.verifyToken], userController.editProfile);

  // delete user
  app.delete("/api/user/:id", [authJwt.verifyToken], userController.deleteUser);

  // update user role
  app.put("/api/user/:id", [authJwt.verifyToken], userController.updateUserRole);

  // get 1 user according to roles
  app.get("/api/test/user", [authJwt.verifyToken], userController.userContent);
  app.get(
    "/api/test/pm",
    [authJwt.verifyToken, authJwt.isPmOrAdmin],
    userController.managementBoard
  );
  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    userController.adminBoard
  );
    //post book
    app.post("/book", [authJwt.verifyToken], bookController.createbook);

    //get book
    app.get("/book", [authJwt.verifyToken], bookController.books);

    //get book id
    app.get("/book/:id", bookController.book);

    //update book by id
    app.put("/book/:id", [authJwt.verifyToken], bookController.update);

    //delete book by id
    app.delete("/book/:id", [authJwt.verifyToken], bookController.deletebook);

    //post order
    app.post("/order", [authJwt.verifyToken], orderController.order);

    //get order
    app.get("/orders", [authJwt.verifyToken], orderController.orders);

    //get order
    app.get("/order/:id", [authJwt.verifyToken], orderController.orderId);

    //post comment
    app.post("/comment/:id", [authJwt.verifyToken], commentController.comment);

    //get all comment
    app.get("/comment", commentController.getComment);

    //get comment by book id
    app.get("/comment/book/:id", [authJwt.verifyToken], commentController.getCommentByIdBook);

  // error handler 404
  app.use(function(req, res, next) {
    return res.status(404).send({
      status: 404,
      message: "Not Found"
    });
  });

  // error handler 500
  app.use(function(err, req, res, next) {
    return res.status(500).send({
      error: err
    });
  });
};
